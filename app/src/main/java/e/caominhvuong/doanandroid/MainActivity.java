package e.caominhvuong.doanandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    EditText edSDT,edMatKhau;
    Spinner spLoaiuser;
    Button btnDangky,btnDangNhap;
    ArrayList<LoaiUser> arrayListLoaiUser;
    ArrayAdapter<LoaiUser> arrayAdapterLoaiUser;
    DatabaseHelper DataBasemanager;
    public void Anhxa()
    {
     edSDT=findViewById(R.id.txtusername);
     edMatKhau=findViewById(R.id.txtpassword);
     spLoaiuser=findViewById(R.id.cmbloaiuser);
     btnDangky=findViewById(R.id.btnDangKi);
     btnDangNhap=findViewById(R.id.btnDangNhap);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Anhxa();
        DataBasemanager=new DatabaseHelper(this,"DSLUSER",null,1);
        arrayListLoaiUser=DataBasemanager.getallLoaiUser();
        arrayAdapterLoaiUser=new ArrayAdapter<LoaiUser>(this,android.R.layout.simple_spinner_item,arrayListLoaiUser);
        arrayAdapterLoaiUser.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spLoaiuser.setAdapter(arrayAdapterLoaiUser);
        btnDangky.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btnDangNhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });



    }

}
