package e.caominhvuong.doanandroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DatabaseHelper  extends SQLiteOpenHelper{
    public static final int DATABASE_VERSION=1;
    private static String CREATE_LOAIUSER = "CREATE TABLE LOAIUSER (MaLoaiUser varchar(10) primary key , TenLoaiUser varchar(100))";
    private static String CREATE_USER = "CREATE TABLE IF NOT EXISTS  USER " +
            "(SoDienThoai varchar(5) primary key,HoTen varchar(100),MatKhau varchar(15)," +
            "MaLoaiUser varchar(10) not null constraint MaLoaiUser references " +
            "LOAIUSER(MaLoaiUser) on delete cascade)";
    public static  String DROP_LOAIUSER = "DROP TABLE LOAIUSER";
    public static String DROP_USER="DROP TABLE USER";



    public void themTable( String strTBLName)
    {
        String s=" CREATE TABLE "+ strTBLName + "(MaHang varchar(10) primary key , TenHang varchar(100))";
        SQLiteDatabase db= getWritableDatabase();
        db.execSQL(s);
    }
    public DatabaseHelper(Context context, String DATABASE_NAME, SQLiteDatabase.CursorFactory factory, int DATABASE_VERSION)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }
    //2 dạng truy vấn
    // khong tra ket quả, create, insert, update, delete..
    //tra ve let quả ==> select ??
    public void QueryData (String sql)
    {
        SQLiteDatabase db=getWritableDatabase();
        db.execSQL(sql);
    }
    public Cursor getData(String sql)
    {
        SQLiteDatabase db=getWritableDatabase();//doc va ghi
        return db.rawQuery(sql, null);
    }


    public ArrayList<LoaiUser> getallLoaiUser(){
        SQLiteDatabase db = getReadableDatabase();
        String sql ="select * from LOAIUSER ";
        Cursor cursor  =db.rawQuery(sql, null);
        //Cursor cursor = db.query("HangSanXuat",null,null,null,null,null,null);
        ArrayList<LoaiUser> list = new ArrayList<LoaiUser>();
        if(cursor != null)
        {
            cursor.moveToFirst();
            while(!cursor.isAfterLast())
            {
                LoaiUser loaiUser=new LoaiUser(cursor.getString(0),cursor.getString(1));
                list.add(loaiUser);
                cursor.moveToNext();
            }
        }
        return list;
    }

    void themCung(SQLiteDatabase db)
    {
        String sql="INSERT INTO LOAIUSER VALUES ('User1','Chủ Salon')";
        db.execSQL(sql);
        String sql1="INSERT INTO LOAIUSER VALUES ('User2','Quản lý')";
        db.execSQL(sql1);
        String sql2="INSERT INTO LOAIUSER VALUES ('User3','Nhân viên')";
        db.execSQL(sql2);
    }
    //Bắt buộc
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String s="CREATE TABLE IF NOT EXISTS  LOAIUSER (MaLoaiUser varchar(10) primary key " +
                ", TenLoaiUser nvarchar(100))";
        db.execSQL(s);
        //db.execSQL(CREATE_HANGSANXUAT);
        db.execSQL(CREATE_USER);
        themCung(db);
    }
    public ArrayList<User> getallUser(){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query("USER",null,null,null,null,null,null);
        ArrayList<User> list = new ArrayList<User>();
        if(cursor != null)
        {
            cursor.moveToFirst();
            while(!cursor.isAfterLast())
            {
                User user=new User(cursor.getString(0),cursor.getString(1),cursor.getString(2),cursor.getString(3));
                list.add(user);
                cursor.moveToNext();
            }
        }
        return list;
    }
    public boolean update(User sp){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put("SoDienThoai",sp.getSoDienThoai());
        values.put("HoTen",sp.getHoTen());
        values.put("MatKhau",sp.getMatkhau());
        String whereClause="SoDienThoai=?";
        String[] whereArgs={sp.getSoDienThoai()};
        long rec=db.update("USER", values, whereClause, whereArgs);
        db.close();
        return rec>0;
    }
    public boolean deleteUser(String Sdt){
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause="SoDienThoai=?";
        String[] whereArgs={Sdt};
        long rec=db.delete("USER", whereClause, whereArgs);
        db.close();
        return rec>0;
    }
    public boolean ThemUser(User sp)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("SoDienThoai",sp.getSoDienThoai());
        values.put("HoTen",sp.getHoTen());
        values.put("MatKhau",sp.getMatkhau());
        values.put("MaLoaiUser",sp.getMaloaiUser());
        db.insert("USER",null,values);
        db.close();
        return true;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String s="DROP TABLE IF EXISTS SANPHAM";
        //if(i!=i1)
        {
            db.execSQL(s);
            //db.execSQL(DROP_SANPHAM);
            db.execSQL(DROP_LOAIUSER);
            onCreate(db);
        }
    }
}
