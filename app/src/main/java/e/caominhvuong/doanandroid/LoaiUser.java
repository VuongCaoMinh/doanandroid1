package e.caominhvuong.doanandroid;

public class LoaiUser {
    public LoaiUser(String maloaiUser, String tenLoaiUser) {
        MaloaiUser = maloaiUser;
        TenLoaiUser = tenLoaiUser;
    }
public  LoaiUser()
{
    MaloaiUser="";
    TenLoaiUser="";
}
    private String MaloaiUser;
    private String TenLoaiUser;
    public String getMaloaiUser() {
        return MaloaiUser;
    }

    public void setMaloaiUser(String maloaiUser) {
        MaloaiUser = maloaiUser;
    }

    public String getTenLoaiUser() {
        return TenLoaiUser;
    }

    public void setTenLoaiUser(String tenLoaiUser) {
        TenLoaiUser = tenLoaiUser;
    }
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LoaiUser loaiUser = (LoaiUser) o;

        return MaloaiUser.equals(loaiUser.MaloaiUser);

    }
    public String toString() {
        return this.getTenLoaiUser();
    }


}
