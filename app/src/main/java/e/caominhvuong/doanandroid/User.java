package e.caominhvuong.doanandroid;

import java.io.Serializable;

public class User implements Serializable {
    private String HoTen;
    private String SoDienThoai;
    private  String Matkhau;

    public String getMaloaiUser() {
        return MaloaiUser;
    }

    public void setMaloaiUser(String maloaiUser) {
        MaloaiUser = maloaiUser;
    }

    private String MaloaiUser;

    public User(String hoTen, String soDienThoai, String matkhau, String maloaiUser) {
        HoTen = hoTen;
        SoDienThoai = soDienThoai;
        Matkhau = matkhau;
        MaloaiUser = maloaiUser;
    }



    public User() {
        HoTen = "";
        SoDienThoai = "";
        Matkhau = "";
        MaloaiUser="";
    }


    public String getHoTen() {
        return HoTen;
    }

    public void setHoTen(String hoTen) {
        HoTen = hoTen;
    }

    public String getSoDienThoai() {
        return SoDienThoai;
    }

    public void setSoDienThoai(String soDienThoai) {
        SoDienThoai = soDienThoai;
    }

    public String getMatkhau() {
        return Matkhau;
    }

    public void setMatkhau(String matkhau) {
        Matkhau = matkhau;
    }
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return SoDienThoai.equals(user.SoDienThoai);

    }
    public int hashCode() {
        return SoDienThoai.hashCode();
    }
    public String toString() {
        return SoDienThoai +" - "+ HoTen;
    }

}
